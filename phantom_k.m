function [ ksignal ] = phantom_k(kx, ky)
% function [ ksignal ] = phantom_k(kx, ky)
%
% Evaluates k-space of Shepp-Logan phantom at specified points.
%
% Based on Java package available from:
% http://www.mathworks.com/matlabcentral/fileexchange/15697
%
% Koay CG, Sarlls JE & Ozarslan E. 
% Three Dimensional Analytical Magnetic Resonance Imaging Phantom in the 
% Fourier Domain. 
% Magn Reson Med. 58:430-436 (2007)
%--------------------------------------------------------------------------
% Parameters
%
% kx, ky: array-like
% Matrices containing the x, y co-ordinates of points to be evaluated. 
%
% Note:
% kx, ky, and kz should be calculated based on the following
% considerations:
% FOV = 2 => delta_k = 0.5 
%--------------------------------------------------------------------------
% Returns
%
% ksignal: array-like 
% Complex-valued k-space values at the specified co-ordinates.
%--------------------------------------------------------------------------
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
%--------------------------------------------------------------------------

try
  ksignal = phantom_k_fast(kx, ky);
catch
  ksignal = phantom_k_slow(kx, ky);
end
