function [ ksignal ] = phantom3_k_fast(kx, ky, kz)
% function [ ksignal ] = phantom3_k_fast(kx, ky, kz)

% Sanity checks
Nk = numel(kx);
if numel(ky) ~= Nk
   error('incompatible shapes between parameters 1 and 2')
end
if numel(kz) ~= Nk
   error('incompatible shapes between parameters 1 and 3')
end

% call java
phantom = javaObject('STBB.SheppLogan3D');
rList = [kx(:), ky(:), kz(:)];
ksig_ri = phantom.FourierDomainSignal(rList);
ksignal = complex(ksig_ri(:, 1), ksig_ri(:, 2));
ksignal = reshape(ksignal, size(kx));
