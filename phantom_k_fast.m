function [ ksignal ] = phantom_k_fast(kx, ky)
% function [ ksignal ] = phantom_k_fast(kx, ky)

% Sanity checks
Nk = numel(kx);
if numel(ky) ~= Nk
   error('incompatible shapes between parameters 1 and 2')
end

% call java
phantom = javaObject('STBB.SheppLogan2D');
rList = [kx(:), ky(:)];
ksig_ri = phantom.FourierDomainSignal(rList);
ksignal = complex(ksig_ri(:, 1), ksig_ri(:, 2));
ksignal = reshape(ksignal, size(kx));
