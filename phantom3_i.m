function [ isignal ] = phantom3_i(ix, iy, iz, varargin)
% function [ isignal ] = phantom_i(ix, iy, iz, [Nchunk, ])
%
% Evaluates image space of Shepp-Logan phantom at specified points.
%
% Based on Java package available from:
% http://www.mathworks.com/matlabcentral/fileexchange/15697
%
% Koay CG, Sarlls JE & Ozarslan E. 
% Three Dimensional Analytical Magnetic Resonance Imaging Phantom in the 
% Fourier Domain. 
% Magn Reson Med. 58:430-436 (2007)
%--------------------------------------------------------------------------
% Parameters
%
% ix, iy, iz: array-like, values in [-1, 1]
% Matrices containing the x, y, z co-ordinates of points to be evaluated.
%
%--------------------------------------------------------------------------
% Returns
%
% isignal: array-like 
% Real image space values at the specified co-ordinates.
%--------------------------------------------------------------------------
% Andrew Aitken <andrew.aitken@kcl.ac.uk>
% revised by Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
%--------------------------------------------------------------------------

try
  isignal = phantom3_i_fast(ix, iy, iz);
catch
  isignal = phantom3_i_slow(ix, iy, iz);
end
