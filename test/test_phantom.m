% phantom_test
N = 64;
FOV = 2;
uj = ((0:N-1) / N - 0.5) * FOV;
kj = ((0:N-1) / N - 0.5) * (1.0 / (FOV / N));

%% 2D
[kx, ky] = meshgrid(kj, kj);
kdat = phantom_k(kx, ky);
img2 = fftshift(ifft2(ifftshift(kdat))) / (FOV ^ 2);
clear('kx', 'ky', 'kdat')

[ux, uy] = meshgrid(uj, uj);
idat = phantom_i(ux, uy);
clear('ux', 'uy')

%% 3D
[kx, ky, kz] = meshgrid(kj, kj, kj);
kdat = phantom3_k(kx, ky, kz, 4);
vol3 = fftshift(ifftn(ifftshift(kdat))) / (FOV ^ 3);
clear('kx', 'ky', 'kz', 'kdat')

[ux, uy, uz] = meshgrid(uj, uj, uj);
vdat = phantom3_i(ux, uy, uz, 4);
clear('ux', 'uy', 'uz')

%% plotting
figure()
title('2-D Shepp-Logan')

subplot(211)
img = idat;
imagesc(img)
ylabel('Image data')
colormap gray
axis equal off

subplot(212)
img = abs(img2);
imagesc(img)
ylabel('Fourier data + FFT')
colormap gray
axis equal off


figure()
title('3-D Shepp-Logan')

subplot(231)
img = squeeze(vdat(N/2, :, :));
imagesc(img)
ylabel('Image data')
colormap gray
axis equal off

subplot(232)
img = squeeze(vdat(:, N/2, :));
imagesc(img)
ylabel('Image data')
colormap gray
axis equal off

subplot(233)
img = squeeze(vdat(:, :, N/2));
imagesc(img)
ylabel('Image data')
colormap gray
axis equal off

subplot(234)
img = squeeze(abs(vol3(N/2, :, :)));
imagesc(img)
ylabel('Fourier data + FFT')
colormap gray
axis equal off

subplot(235)
img = squeeze(abs(vol3(:, N/2, :)));
imagesc(img)
ylabel('Fourier data + FFT')
colormap gray
axis equal off

subplot(236)
img = squeeze(abs(vol3(:, :, N/2)));
imagesc(img)
ylabel('Fourier data + FFT')
colormap gray
axis equal off
