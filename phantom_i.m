function [ isignal ] = phantom_i(ix, iy)
% function [ isignal ] = phantom_i(ix, iy)
%
% Evaluates image space of Shepp-Logan phantom at specified points.
%
% Based on Java package available from:
% http://www.mathworks.com/matlabcentral/fileexchange/15697
%
% Koay CG, Sarlls JE & Ozarslan E. 
% Three Dimensional Analytical Magnetic Resonance Imaging Phantom in the 
% Fourier Domain. 
% Magn Reson Med. 58:430-436 (2007)
%--------------------------------------------------------------------------
% Parameters
%
% ix, iy: array-like, values in [-1, 1]
% Matrices containing the x, y co-ordinates of points to be evaluated.
%
%--------------------------------------------------------------------------
% Returns
%
% isignal: array-like 
% Real image space values at the specified co-ordinates.
%--------------------------------------------------------------------------
% Andrew Aitken <andrew.aitken@kcl.ac.uk>
% revised by Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
%--------------------------------------------------------------------------

try
  isignal = phantom_i_fast(ix, iy);
catch
  isignal = phantom_i_slow(ix, iy);
end
