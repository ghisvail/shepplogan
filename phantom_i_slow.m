function [ isignal ] = phantom_i_slow(ix, iy)
% function [ isignal ] = phantom_i_slow(ix, iy)

% Sanity checks
Nk = numel(ix);
if numel(iy) ~= Nk
   error('incompatible shapes between parameters 1 and 2')
end

% call java
phantom = javaObject('STBB.SheppLogan2D');
isignal = zeros(size(ix));
for k = 1:Nk
  isignal(k) = phantom.ImageDomainSignal(ix(k), iy(k));
end
