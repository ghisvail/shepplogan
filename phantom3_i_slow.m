function [ isignal ] = phantom3_i_slow(ix, iy, iz)
% function [ isignal ] = phantom3_i_slow(ix, iy, iz)

% Sanity checks
Nk = numel(ix);
if numel(iy) ~= Nk
   error('incompatible shapes between parameters 1 and 2')
end
if numel(iz) ~= Nk
   error('incompatible shapes between parameters 1 and 3')
end

% call java
phantom = javaObject('STBB.SheppLogan3D');
isignal = zeros(size(ix));
for k = 1:Nk
  isignal(k) = phantom.ImageDomainSignal(ix(k), iy(k), iz(k));
end
