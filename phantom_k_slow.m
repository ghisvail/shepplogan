function [ ksignal ] = phantom_k_slow(kx, ky)
% function [ ksignal ] = phantom_k_slow(kx, ky)

% Sanity checks
Nk = numel(kx);
if numel(ky) ~= Nk
   error('incompatible shapes between parameters 1 and 2')
end

% call java
phantom = javaObject('STBB.SheppLogan2D');
ksig_real = zeros(size(kx));
ksig_imag = zeros(size(kx));
for k = 1:Nk
  ksig_ri = phantom.FourierDomainSignal(kx(k), ky(k));
  ksig_real(k) = ksig_ri(1);
  ksig_imag(k) = ksig_ri(2);
end
ksignal = complex(ksig_real, ksig_imag);
