function [ isignal ] = phantom3_i_fast(ix, iy, iz)
% function [ isignal ] = phantom3_i_fast(ix, iy, iz)

% Sanity checks
Nk = numel(ix);
if numel(iy) ~= Nk
   error('incompatible shapes between parameters 1 and 2')
end
if numel(iz) ~= Nk
   error('incompatible shapes between parameters 1 and 3')
end

% call java
phantom = javaObject('STBB.SheppLogan3D');
rList = [ix(:), iy(:), iz(:)];
isignal = phantom.ImageDomainSignal(rList);
isignal = reshape(isignal, size(ix));
