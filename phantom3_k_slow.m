function [ ksignal ] = phantom3_k_slow(kx, ky, kz)
% function [ ksignal ] = phantom3_k_slow(kx, ky, kz)

% Sanity checks
Nk = numel(kx);
if numel(ky) ~= Nk
   error('incompatible shapes between parameters 1 and 2')
end
if numel(kz) ~= Nk
   error('incompatible shapes between parameters 1 and 3')
end

% call java
phantom = javaObject('STBB.SheppLogan3D');
ksig_real = zeros(size(kx));
ksig_imag = zeros(size(kx));
for k = 1:Nk
  ksig_ri = phantom.FourierDomainSignal(kx(k), ky(k), kz(k));
  ksig_real(k) = ksig_ri(1);
  ksig_imag(k) = ksig_ri(2);
end
ksignal = complex(ksig_real, ksig_imag);
