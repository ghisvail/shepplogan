function [ isignal ] = phantom_i_fast(ix, iy)
% function [ isignal ] = phantom_i_fast(ix, iy)

% Sanity checks
Nk = numel(ix);
if numel(iy) ~= Nk
   error('incompatible shapes between parameters 1 and 2')
end

% call java
phantom = javaObject('STBB.SheppLogan2D');
rList = [ix(:), iy(:)];
isignal = phantom.ImageDomainSignal(rList);
isignal = reshape(isignal, size(ix));
